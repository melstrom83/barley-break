#include "stdafx.h"

#include <ctime>

#include <osgViewer/Viewer>

#include "AppEventHandler.h"



AppGridHandler::AppGridHandler(float tileSize/* = 0.8f*/, int tileCount /*= 4*/)
  : tileCount_(tileCount)
  , tileSize_(tileSize)
  , tileColor_(1.0f, 0.0f, .0f, 1.0f)
  , tileMargin_(tileSize * 0.01f)
  , tileOrigin_(0.0f, 0.0f, 0.0f)
{
  root_ = new osg::Group();

  tileContainer_.clear();
  tileContainer_.resize(tileCount_ * tileCount_);

  createTiles();
  updateTiles();
}


osg::ref_ptr<osg::PositionAttitudeTransform> AppGridHandler::getTile(const std::pair<int, int>& location)
{
  auto x = std::get<0>(location);
  auto y = std::get<1>(location);
  return tileContainer_[x + y * tileCount_];
}

void AppGridHandler::setTile(const std::pair<int, int>& location, osg::ref_ptr<osg::PositionAttitudeTransform> tile)
{
  auto x = std::get<0>(location);
  auto y = std::get<1>(location);
  tileContainer_[x + y * tileCount_] = tile;
}


void AppGridHandler::createTiles()
{
  tileContainer_.clear();
  tileContainer_.resize(tileCount_ * tileCount_);

  std::srand(std::time(nullptr));

  for (int i = 0; i < tileCount_ * tileCount_ - 1; ++i)
  {
    const osg::ref_ptr< osg::PositionAttitudeTransform> transform = new osg::PositionAttitudeTransform();
    transform->setPosition(tileOrigin_);
    root_->addChild(transform);

    const osg::ref_ptr<osg::Geode> geode = new osg::Geode();
    transform->addChild(geode);

    osg::Vec3 tilePosition(0.0f, 0.0f, 0.0f);
    const osg::ref_ptr<osg::Box> box = new osg::Box(tilePosition, tileSize_, 0.01f * tileSize_, tileSize_);
    const osg::ref_ptr<osg::ShapeDrawable> drawable = new osg::ShapeDrawable(box.get());
    drawable->setName("plate" + std::to_string(i + 1));
    drawable->setColor(tileColor_);
    geode->addDrawable(drawable);

    const osg::ref_ptr<osgText::Text> text = new osgText::Text();
    text->setName("label" + std::to_string(i + 1));
    text->setCharacterSize(0.5f);
    text->setCharacterSizeMode(osgText::Text::CharacterSizeMode::OBJECT_COORDS);
    text->setAutoRotateToScreen(true);
    text->setFont(0);
    text->setFontResolution(64, 64);
    text->setText(std::to_string(i + 1));
    text->setAxisAlignment(osgText::Text::XZ_PLANE);
    text->setDrawMode(osgText::Text::TEXT);
    text->setAlignment(osgText::Text::CENTER_CENTER);
    text->setShaderTechnique(osgText::ShaderTechnique::ALL_FEATURES);
    text->setPosition(osg::Vec3(0, -0.011f * tileSize_, -0.011f * tileSize_));
    text->setColor(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));
    geode->addDrawable(text);

    auto fnPickIndex = [this]()
    {
      do
      {
        const float rand = static_cast<float>(std::rand()) / RAND_MAX;
        const size_t index = static_cast<size_t>(rand * tileCount_ * tileCount_);

        if (tileContainer_[index] == nullptr)
        {
          return index;
        }

      } while (true);
    };

    const auto tileIndex = fnPickIndex();
    tileContainer_[tileIndex] = transform;
  }

}

void AppGridHandler::updateTiles()
{
  for (int i = 0; i < tileContainer_.size(); ++i)
  {
    auto tile = tileContainer_[i];
    
    if (tile == nullptr)
    {
      emptyTile = i;
      continue;
    }

    auto x = i % tileCount_;
    auto y = i / tileCount_;

    const float sizeX = (tileSize_ + 2.0f * tileMargin_) * tileCount_;
    const float sizeY = (tileSize_ + 2.0f * tileMargin_) * tileCount_;
    const float offsetX = x * (tileSize_ + 2.0f * tileMargin_) - 0.5f * sizeX;
    const float offsetY = 0.5f * sizeY - y * (tileSize_ + 2.0f * tileMargin_);

    tile->setPosition(osg::Vec3(offsetX, 0.0f, offsetY));
  }
}

bool AppGridHandler::handle(osgGA::Event* _event, osg::Object* _object, osg::NodeVisitor* _visitor)
{
  auto eventAdapter = _event->asGUIEventAdapter();
  auto eventType = eventAdapter->getEventType();

  auto actionAdapter = _visitor->asEventVisitor()->getActionAdapter();
  auto actionView = dynamic_cast<osgViewer::View*>(actionAdapter->asView());

  if (eventType & osgGA::GUIEventAdapter::EventType::DOUBLECLICK)
  {
    osgUtil::LineSegmentIntersector::Intersections intersections;
    if (!actionView->computeIntersections(*eventAdapter, intersections))
    {
      return true;
    }

    for (osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin(); hitr != intersections.end(); ++hitr)
    {
      if (!hitr->drawable.valid())
      {
        return true;
      }

      for (int i = 0; i < tileCount_; ++i)
      {
        for (int j = 0; j < tileCount_; ++j)
        {
          auto node = getTile({ i, j });
          if (hitr->drawable->getParent(0)->getParent(0) == node)
          {
            auto x = emptyTile % tileCount_;
            auto y = emptyTile / tileCount_;

            if(  ((i == x + 1) && (j == y))
              || ((i == x - 1) && (j == y))
              || ((j == y + 1) && (i == x))
              || ((j == y - 1) && (i == x)))
            {
              setTile({ i, j }, nullptr);
              setTile({ x, y }, node);
              updateTiles();
              return false;
            }
          }
        }
      }
    }
  }

  return true;
}

