#pragma once

#include <osgViewer/Viewer>
#include <osg/ShapeDrawable>
#include <osg/Depth>
#include <osg/CullFace>
#include <osg/Shader>
#include <osg/PositionAttitudeTransform>
#include <osg/FragmentProgram>
#include <osg/VertexProgram>
#include <osg/Program>
#include <osg/Uniform>
#include <osgText/Text>
#include <osgGA/TrackballManipulator>


class AppGridHandler : public osgGA::EventHandler
{
public:
  AppGridHandler(float tileSize = 0.8f, int tileCount = 4);
  ~AppGridHandler() {};

  const osg::ref_ptr<osg::Group>& getRoot() const { return root_; }

  osg::ref_ptr<osg::PositionAttitudeTransform> getTile(const std::pair<int, int>& location);
  void setTile(const std::pair<int, int>& location, osg::ref_ptr<osg::PositionAttitudeTransform> tile);

private:
  void createTiles();
  void updateTiles();

  bool handle(osgGA::Event* event, osg::Object* object, osg::NodeVisitor* visitor) override;

private:
  std::vector<osg::ref_ptr<osg::PositionAttitudeTransform>> tileContainer_;

  osg::Vec3 tileOrigin_{ 0.0f, 0.0f, 0.0f };
  int tileCount_{ 0 };
  float tileSize_{ 0.0f };
  float tileMargin_{ 0.0f };
  osg::Vec4 tileColor_{ 0.0f, 0.0f, 0.0f, 0.0f };

  int emptyTile{ 0 };

  osg::ref_ptr<osg::Group> root_{ nullptr };
};
