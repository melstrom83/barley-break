﻿// TestApp15.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <osgViewer/Viewer>
#include <osg/ShapeDrawable>
#include <osg/Depth>
#include <osg/CullFace>
#include <osg/Shader>
#include <osg/PositionAttitudeTransform>
#include <osg/FragmentProgram>
#include <osg/VertexProgram>
#include <osg/Program>
#include <osg/Uniform>
#include <osgText/Text>
#include <osgGA/TrackballManipulator>

#include "AppEventHandler.h"

int main(){

  // construct the viewer.
  osgViewer::Viewer viewer;
  
  viewer.setThreadingModel(osgViewer::Viewer::SingleThreaded);
  viewer.setUpViewInWindow(100, 100, 500, 500);
  viewer.setName("viewer");

  const osg::ref_ptr<AppGridHandler> gridHandler = new AppGridHandler(0.8f, 4);
  viewer.setSceneData(gridHandler->getRoot());
  viewer.addEventHandler(gridHandler);
  

  // create the windows and run the threads.
  return viewer.run();
   
}