# TestApp15

## How to compile on Windows with VS 2017

* Clone the repository
* Download OpenSceneGraph binaries from here: http://objexx.com/OpenSceneGraph.html 
  (OpenSceneGraph 3.4.1 -- Visual C++ 2017 -- 64-bit -- Release, and  OpenSceneGraph 3.4.1 -- Visual C++ 2017 -- 64-bit -- Debug)
* Extract OpenSceneGraph-3.4.1-VC2017-64-Release.7z into /thirdparty/OpenSceneGraph/3.4.1/Release
* Extract OpenSceneGraph-3.4.1-VC2017-64-Debug.7z into /thirdparty/OpenSceneGraph/3.4.1/Debug
* Open TestApp15.sln
